const express = require('express')
const { getSdk } = require('balena-sdk')
const cors = require('cors')

/* Constants */
const app = express()

/* Balena SDK setup */
const balena = getSdk({
  apiUrl: 'https://api.balena.coolab.org/',
})

/* Express middlewares */
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

/* API */
app.post('/auth/login', async (req, res) => {
  const { email, password } = req.body
  try {
    await balena.auth.login({ email, password })
    const token = await balena.auth.getToken()
    res.json({ token })
  } catch (err) {
    console.error(err)
    res.status(401).send('Bad credentials')
  }
})

app.post('/auth/logout', (req, res) => {
  res.status(200).clearCookie('auth', {
    path: '/',
  })
  res.redirect('/')
})

app.get('/deviceTypes', async (req, res) => {
  try {
    const deviceTypes = await balena.models.deviceType.getAll()
    res.json(deviceTypes)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

app.get('/fleets', async (req, res) => {
  try {
    const fleets =
      await balena.models.application.getAllWithDeviceServiceDetails()
    res.json(fleets)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

app.get('/devices', async (req, res) => {
  try {
    const devices = await balena.models.device.getAll()
    res.json(devices)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

app.get('/deviceType/:id', async (req, res) => {
  const { id } = req.params
  try {
    const deviceType = await balena.models.deviceType.get(parseInt(id))
    res.json(deviceType)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

app.get('/fleet/:id', async (req, res) => {
  const { id } = req.params
  try {
    const fleet = await balena.models.application.getWithDeviceServiceDetails(
      parseInt(id)
    )
    res.json(fleet)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

app.get('/device/:id', async (req, res) => {
  const { id } = req.params
  try {
    const device = await balena.models.device.get(id)
    res.json(device)
  } catch (err) {
    console.error(err)
    res.status(500)
  }
})

export default {
  path: '/api',
  handler: app,
}
